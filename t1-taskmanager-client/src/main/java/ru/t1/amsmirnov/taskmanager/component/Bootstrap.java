package ru.t1.amsmirnov.taskmanager.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.amsmirnov.taskmanager.api.endpoint.*;
import ru.t1.amsmirnov.taskmanager.api.repository.ICommandRepository;
import ru.t1.amsmirnov.taskmanager.api.service.ICommandService;
import ru.t1.amsmirnov.taskmanager.api.service.ILoggerService;
import ru.t1.amsmirnov.taskmanager.api.service.IPropertyService;
import ru.t1.amsmirnov.taskmanager.api.service.IServiceLocator;
import ru.t1.amsmirnov.taskmanager.client.*;
import ru.t1.amsmirnov.taskmanager.command.AbstractCommand;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.system.ArgumentNotSupportedException;
import ru.t1.amsmirnov.taskmanager.exception.system.CommandNotSupportedException;
import ru.t1.amsmirnov.taskmanager.repository.CommandRepository;
import ru.t1.amsmirnov.taskmanager.service.CommandService;
import ru.t1.amsmirnov.taskmanager.service.LoggerService;
import ru.t1.amsmirnov.taskmanager.service.PropertyService;
import ru.t1.amsmirnov.taskmanager.util.SystemUtil;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.amsmirnov.taskmanager.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    @Getter
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    @Getter
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IAuthEndpointClient authEndpoint = new AuthEndpointClient();

    @NotNull
    private final ISystemEndpointClient systemEndpoint = new SystemEndpointClient();

    @NotNull
    private final IDomainEndpointClient domainEndpoint = new DomainEndpointClient();

    @NotNull
    private final IProjectEndpointClient projectEndpoint = new ProjectEndpointClient();

    @NotNull
    private final ITaskEndpointClient taskEndpoint = new TaskEndpointClient();

    @NotNull
    private final IUserEndpointClient userEndpoint = new UserEndpointClient();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    private void initApplication() {
        try {
            initPID();
            loggerService.info("** WELCOME TO TASK MANAGER **");
            Runtime.getRuntime().addShutdownHook(new Thread(this::shutdownApplication));
            fileScanner.start();
        } catch (final Exception exception) {
            renderError(exception);
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        @NotNull final String fileName = "taskmanager(" + pid + ").pid";
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }


    private void shutdownApplication() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    private void renderError(@NotNull Exception exception) {
        loggerService.error(exception);
        System.out.println("[FAIL]");
    }

    public void start(@Nullable String[] args) {
        if (processArguments(args)) exit();
        initApplication();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND: ");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception exception) {
                renderError(exception);
            }
        }
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        getCommandService().add(command);
    }

    public boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String argument = args[0];
        try {
            processArgument(argument);
        } catch (@NotNull final Exception exception) {
            renderError(exception);
        }
        return true;
    }

    public void processArgument(@Nullable final String argument) throws AbstractException {
        @Nullable final AbstractCommand command = commandService.getCommandByArgument(argument);
        if (command == null) throw new ArgumentNotSupportedException(argument);
        command.execute();
    }

    public void processCommand(@NotNull final String commandName) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(commandName);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        abstractCommand.execute();
    }

    public void exit() {
        System.exit(0);
    }

    @NotNull
    @Override
    public IAuthEndpointClient getAuthEndpoint() {
        return authEndpoint;
    }

    @NotNull
    @Override
    public ISystemEndpointClient getSystemEndpoint() {
        return systemEndpoint;
    }

    @NotNull
    @Override
    public IDomainEndpointClient getDomainEndpoint() {
        return domainEndpoint;
    }

    @NotNull
    @Override
    public IProjectEndpointClient getProjectEndpoint() {
        return projectEndpoint;
    }

    @NotNull
    @Override
    public ITaskEndpointClient getTaskEndpoint() {
        return taskEndpoint;
    }

    @NotNull
    @Override
    public IUserEndpointClient getUserEndpoint() {
        return userEndpoint;
    }

}
