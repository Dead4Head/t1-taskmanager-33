package ru.t1.amsmirnov.taskmanager.command.server;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public class DisconnectCommand extends AbstractServerCommand {

    @NotNull
    public static final String NAME = "disconnect";

    @NotNull
    public static final String DESCRIPTION = "Disconnect from server.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        try {
            System.out.println("[DISCONNECTION FROM SERVER]");
            getServiceLocator().getAuthEndpoint().disconnect();
            getServiceLocator().getAuthEndpoint().setSocket(null);
            getServiceLocator().getDomainEndpoint().setSocket(null);
            getServiceLocator().getProjectEndpoint().setSocket(null);
            getServiceLocator().getSystemEndpoint().setSocket(null);
            getServiceLocator().getTaskEndpoint().setSocket(null);
            getServiceLocator().getUserEndpoint().setSocket(null);
        } catch (final Exception exception) {
            getServiceLocator().getLoggerService().error(exception);
        }
    }

}
