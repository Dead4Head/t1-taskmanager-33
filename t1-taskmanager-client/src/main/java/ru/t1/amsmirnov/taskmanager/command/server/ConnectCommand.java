package ru.t1.amsmirnov.taskmanager.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

import java.net.Socket;

public class ConnectCommand extends AbstractServerCommand {

    @NotNull
    public static final String NAME = "connect";

    @NotNull
    public static final String DESCRIPTION = "Connect to server.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        try {
            System.out.println("[CONNECTION TO SERVER]");
            getServiceLocator().getAuthEndpoint().connect();
            final @Nullable Socket socket = getServiceLocator().getAuthEndpoint().getSocket();
            getServiceLocator().getAuthEndpoint().setSocket(socket);
            getServiceLocator().getDomainEndpoint().setSocket(socket);
            getServiceLocator().getProjectEndpoint().setSocket(socket);
            getServiceLocator().getSystemEndpoint().setSocket(socket);
            getServiceLocator().getTaskEndpoint().setSocket(socket);
            getServiceLocator().getUserEndpoint().setSocket(socket);
        } catch (final Exception exception) {
            getServiceLocator().getLoggerService().error(exception);
        }
    }

}
