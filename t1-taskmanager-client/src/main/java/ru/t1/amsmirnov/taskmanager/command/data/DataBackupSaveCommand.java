package ru.t1.amsmirnov.taskmanager.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataBase64SaveRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataBase64SaveResponse;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

public final class DataBackupSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-save";

    @NotNull
    public static final String DESCRIPTION = "Save data to backup file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[BACKUP DATA SAVE]");
        @NotNull final DataBase64SaveRequest request = new DataBase64SaveRequest();
        @NotNull final DataBase64SaveResponse response = getDomainEndpoint().saveDataBase64(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
