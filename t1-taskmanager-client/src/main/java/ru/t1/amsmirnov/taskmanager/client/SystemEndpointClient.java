package ru.t1.amsmirnov.taskmanager.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.endpoint.ISystemEndpointClient;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerAboutRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerVersionRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerAboutResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerVersionResponse;

public class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpointClient {

    public SystemEndpointClient() {
    }

    @NotNull
    @Override
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest serverAboutRequest) {
        return call(serverAboutRequest, ServerAboutResponse.class);
    }

    @NotNull
    @Override
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest serverVersionRequest) {
        return call(serverVersionRequest, ServerVersionResponse.class);
    }

}
