package ru.t1.amsmirnov.taskmanager.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.endpoint.IDomainEndpointClient;
import ru.t1.amsmirnov.taskmanager.dto.request.data.*;
import ru.t1.amsmirnov.taskmanager.dto.request.user.UserLoginRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.*;

public class DomainEndpointClient extends AbstractEndpointClient implements IDomainEndpointClient {

    public DomainEndpointClient() {
    }

    public DomainEndpointClient(@NotNull AuthEndpointClient client) {
        super(client);
    }

    @NotNull
    @Override
    public DataBase64LoadResponse loadDataBase64(@NotNull final DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @NotNull
    @Override
    public DataBase64SaveResponse saveDataBase64(@NotNull final DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @NotNull
    @Override
    public DataBinLoadResponse loadDataBin(@NotNull final DataBinLoadRequest request) {
        return call(request, DataBinLoadResponse.class);
    }

    @NotNull
    @Override
    public DataBinSaveResponse saveDataBin(@NotNull final DataBinSaveRequest request) {
        return call(request, DataBinSaveResponse.class);
    }

    @NotNull
    @Override
    public DataJsonLoadFasterXMLResponse loadDataJsonFasterXML(@NotNull final DataJsonLoadFasterXMLRequest request) {
        return call(request, DataJsonLoadFasterXMLResponse.class);
    }

    @NotNull
    @Override
    public DataJsonSaveFasterXMLResponse saveDataJsonFasterXML(@NotNull final DataJsonSaveFasterXMLRequest request) {
        return call(request, DataJsonSaveFasterXMLResponse.class);
    }

    @NotNull
    @Override
    public DataJsonLoadJaxbResponse loadDataJsonJaxB(@NotNull final DataJsonLoadJaxbRequest request) {
        return call(request, DataJsonLoadJaxbResponse.class);
    }

    @NotNull
    @Override
    public DataJsonSaveJaxbResponse saveDataJsonJaxB(@NotNull final DataJsonSaveJaxbRequest request) {
        return call(request, DataJsonSaveJaxbResponse.class);
    }

    @NotNull
    @Override
    public DataXmlLoadFasterXMLResponse loadDataXmlFasterXML(@NotNull final DataXmlLoadFasterXMLRequest request) {
        return call(request, DataXmlLoadFasterXMLResponse.class);
    }

    @NotNull
    @Override
    public DataXmlSaveFasterXMLResponse saveDataXmlFasterXML(@NotNull final DataXmlSaveFasterXMLRequest request) {
        return call(request, DataXmlSaveFasterXMLResponse.class);
    }

    @NotNull
    @Override
    public DataXmlLoadJaxBResponse loadDataXmlJaxB(@NotNull final DataXmlLoadJaxBRequest request) {
        return call(request, DataXmlLoadJaxBResponse.class);
    }

    @NotNull
    @Override
    public DataXmlSaveJaxBResponse saveDataXmlJaxB(@NotNull final DataXmlSaveJaxBRequest request) {
        return call(request, DataXmlSaveJaxBResponse.class);
    }

    @NotNull
    @Override
    public DataYamlLoadFasterXMLResponse loadDataYAMLFasterXML(@NotNull final DataYamlLoadFasterXMLRequest request) {
        return call(request, DataYamlLoadFasterXMLResponse.class);
    }

    @NotNull
    @Override
    public DataYamlSaveFasterXMLResponse saveDataYAMLFasterXML(@NotNull final DataYamlSaveFasterXMLRequest request) {
        return call(request, DataYamlSaveFasterXMLResponse.class);
    }

}
