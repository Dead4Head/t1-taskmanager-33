package ru.t1.amsmirnov.taskmanager.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataXmlLoadJaxBRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataXmlLoadJaxBResponse;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

public final class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Load data from XML file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD XML]");
        @NotNull final DataXmlLoadJaxBRequest request = new DataXmlLoadJaxBRequest();
        @NotNull final DataXmlLoadJaxBResponse response = getDomainEndpoint().loadDataXmlJaxB(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
