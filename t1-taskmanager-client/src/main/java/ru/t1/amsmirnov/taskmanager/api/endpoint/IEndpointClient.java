package ru.t1.amsmirnov.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.net.Socket;

public interface IEndpointClient {

    void connect() throws IOException;

    void disconnect() throws IOException;

    @Nullable Socket getSocket();

    void setSocket(@Nullable Socket socket);

    @NotNull String getHost();

    void setHost(@NotNull String host);
    @NotNull Integer getPort();

    void setPort(@NotNull Integer port);

}
