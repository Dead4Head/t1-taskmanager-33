package ru.t1.amsmirnov.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.task.TaskClearRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.task.TaskClearResponse;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-clear";

    @NotNull
    public static final String DESCRIPTION = "Clear task list.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CLEAR TASK LIST]");
        @NotNull final TaskClearRequest request = new TaskClearRequest();
        @NotNull final TaskClearResponse response = getTaskEndpoint().removeAllTasks(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
