package ru.t1.amsmirnov.taskmanager.api.endpoint;

public interface ITaskEndpointClient extends IEndpointClient, ITaskEndpoint {

}
