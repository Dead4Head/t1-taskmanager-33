package ru.t1.amsmirnov.taskmanager.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataXmlLoadFasterXMLRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataXmlLoadFasterXMLResponse;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

public final class DataXmlLoadFasterXMLCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml";

    @NotNull
    public static final String DESCRIPTION = "Load data from XML file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA LOAD XML]");
        @NotNull final DataXmlLoadFasterXMLRequest request = new DataXmlLoadFasterXMLRequest();
        @NotNull final DataXmlLoadFasterXMLResponse response = getDomainEndpoint().loadDataXmlFasterXML(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
