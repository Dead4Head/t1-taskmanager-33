package ru.t1.amsmirnov.taskmanager.api.endpoint;

public interface ISystemEndpointClient extends IEndpointClient, ISystemEndpoint {

}
