package ru.t1.amsmirnov.taskmanager.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataJsonSaveJaxbRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataJsonSaveJaxbResponse;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

public final class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-json-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Save data to JSON file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DATA SAVE JSON]");
        @NotNull final DataJsonSaveJaxbRequest request = new DataJsonSaveJaxbRequest();
        @NotNull final DataJsonSaveJaxbResponse response = getDomainEndpoint().saveDataJsonJaxB(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
