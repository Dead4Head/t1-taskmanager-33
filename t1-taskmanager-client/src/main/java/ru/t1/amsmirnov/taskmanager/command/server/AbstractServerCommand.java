package ru.t1.amsmirnov.taskmanager.command.server;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.command.AbstractCommand;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;

public abstract class AbstractServerCommand extends AbstractCommand {

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return null;
    }

}
