package ru.t1.amsmirnov.taskmanager.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerAboutRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerVersionRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerAboutResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.net.MalformedURLException;

@WebService
public interface ISystemEndpoint extends IEndpoint {

    @NotNull String NAME = "SystemEndpoint";

    @NotNull String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance() throws MalformedURLException {
        return IEndpoint.newInstance(NAME, PART, ISystemEndpoint.class);
    }

    @NotNull
    @WebMethod
    ServerAboutResponse getAbout(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ServerAboutRequest serverAboutRequest);

    @NotNull
    @WebMethod
    ServerVersionResponse getVersion(@WebParam(name = REQUEST, partName = REQUEST) @NotNull ServerVersionRequest serverVersionRequest);

    @WebMethod(exclude = true)
    public static void main(String[] args) {
        try {
            ServerAboutResponse response1 = ISystemEndpoint.newInstance().getAbout(new ServerAboutRequest());
            System.out.println(response1.getName());
            System.out.println(response1.getEmail());
            ServerVersionResponse response2 = ISystemEndpoint.newInstance().getVersion(new ServerVersionRequest());
            System.out.println(response2.getVersion());
        } catch (final Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
