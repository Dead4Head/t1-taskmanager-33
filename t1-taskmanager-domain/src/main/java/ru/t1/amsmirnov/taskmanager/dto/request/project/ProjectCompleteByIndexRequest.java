package ru.t1.amsmirnov.taskmanager.dto.request.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class ProjectCompleteByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public ProjectCompleteByIndexRequest() {
    }

    public ProjectCompleteByIndexRequest(@Nullable final Integer index) {
        this.index = index;
    }

    @Nullable
    public Integer getIndex() {
        return index;
    }

    public void setIndex(@Nullable final Integer index) {
        this.index = index;
    }
    
}
