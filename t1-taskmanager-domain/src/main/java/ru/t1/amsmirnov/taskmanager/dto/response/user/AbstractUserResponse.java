package ru.t1.amsmirnov.taskmanager.dto.response.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;
import ru.t1.amsmirnov.taskmanager.model.User;

public abstract class AbstractUserResponse extends AbstractResultResponse {

    @Nullable
    private User user;

    public AbstractUserResponse() {
    }

    protected AbstractUserResponse(@Nullable final User user) {
        this.user = user;
    }

    protected AbstractUserResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

    @Nullable
    public User getUser() {
        return user;
    }

    public void setUser(@Nullable final User user) {
        this.user = user;
    }

}