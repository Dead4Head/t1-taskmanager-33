package ru.t1.amsmirnov.taskmanager.dto.request;

import org.jetbrains.annotations.Nullable;

public abstract class AbstractUserRequest extends AbstractRequest {

    @Nullable
    protected String userId;

    @Nullable
    public String getUserId() {
        return this.userId;
    }

    public void setUserId(@Nullable final String userId) {
        this.userId = userId;
    }

}
