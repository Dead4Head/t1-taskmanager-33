package ru.t1.amsmirnov.taskmanager.dto.response.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;
import ru.t1.amsmirnov.taskmanager.model.Task;

import java.util.List;

public final class TaskListResponse extends AbstractResultResponse {

    public TaskListResponse() {
    }

    @Nullable
    private List<Task> tasks;

    public TaskListResponse(@Nullable final List<Task> tasks) {
        this.tasks = tasks;
    }

    public TaskListResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

    @Nullable
    public List<Task> getTasks() {
        return tasks;
    }

}