package ru.t1.amsmirnov.taskmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.model.IWBS;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;

import java.util.Date;

public final class Project extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    @Getter
    @Setter
    private String name = "";

    @Nullable
    @Getter
    @Setter
    private String description = "";

    @NotNull
    @Getter
    @Setter
    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date created = new Date();

    public Project() {
    }

    public Project(
            @NotNull final String name,
            @NotNull final String description,
            @NotNull final Status status
    ) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    @Override
    @NotNull
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(@NotNull final Date created) {
        this.created = created;
    }

}
