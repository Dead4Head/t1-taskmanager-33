package ru.t1.amsmirnov.taskmanager.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

@NoArgsConstructor
public final class UserUnlockResponse extends AbstractResultResponse {

    public UserUnlockResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}