package ru.t1.amsmirnov.taskmanager.dto.request.project;

import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.dto.request.AbstractUserRequest;

public final class ProjectStartByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectStartByIdRequest() {
    }

    public ProjectStartByIdRequest(@Nullable final String id) {
        this.id = id;
    }

    @Nullable
    public String getId() {
        return id;
    }

    public void setId(@Nullable final String id) {
        this.id = id;
    }

}
