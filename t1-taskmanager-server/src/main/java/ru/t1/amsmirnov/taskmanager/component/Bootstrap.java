package ru.t1.amsmirnov.taskmanager.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.api.endpoint.*;
import ru.t1.amsmirnov.taskmanager.api.repository.IProjectRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.IUserRepository;
import ru.t1.amsmirnov.taskmanager.api.service.*;
import ru.t1.amsmirnov.taskmanager.endpoint.*;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.model.User;
import ru.t1.amsmirnov.taskmanager.repository.ProjectRepository;
import ru.t1.amsmirnov.taskmanager.repository.TaskRepository;
import ru.t1.amsmirnov.taskmanager.repository.UserRepository;
import ru.t1.amsmirnov.taskmanager.service.*;
import ru.t1.amsmirnov.taskmanager.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    @Getter
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    @Getter
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    @Getter
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    @Getter
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    @Getter
    private final IUserService userService = new UserService(userRepository, projectService, taskService, propertyService);

    @NotNull
    @Getter
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    @Getter
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    {
        registry(systemEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        loggerService.info("url: " + url);
        Endpoint.publish(url, endpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        @NotNull final String fileName = "taskmanager(" + pid + ").pid";
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initDemoData() {
        try {
            @NotNull User admin = userService.create("admin", "admin", Role.ADMIN);
            @NotNull User test = userService.create("test", "test", "test@test.test");
            @NotNull User user = userService.create("user", "user");

            projectService.create(test.getId(), "Project 1", "Description 1");
            projectService.create(test.getId(), "Project 2", "Description 2");
            projectService.create(test.getId(), "Project 3", "Description 3");
            projectService.create(admin.getId(), "Admin project", "Description of the admin project");

            taskService.create(test.getId(), "TASK 1", "TASK 1");
            taskService.create(admin.getId(), "TASK 2", "TASK 2");
            taskService.create(user.getId(), "TASK 3", "TASK 3");

        } catch (@NotNull final Exception exception) {
            renderError(exception);
        }
    }

    private void renderError(@NotNull Exception exception) {
        loggerService.error(exception);
        System.out.println("[FAIL]");
    }

    public void start() {
        try {
            initPID();
            initDemoData();
            loggerService.info("** WELCOME TO TASK MANAGER **");
            Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
            backup.start();
        } catch (final Exception exception) {
            renderError(exception);
        }
    }

    public void stop() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        backup.stop();
    }

}
